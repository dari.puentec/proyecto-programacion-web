
package com.mycompany.MANDOWEB.Controlles;

import com.mycompany.MANDOWEB.DAO.ComentariosDAO;
import com.mycompany.MANDOWEB.DAO.FavsDAO;
import com.mycompany.MANDOWEB.DAO.NoticiasDAO;
import com.mycompany.MANDOWEB.DAO.UsuarioDAO;
import com.mycompany.MANDOWEB.Models.Comentarios;
import com.mycompany.MANDOWEB.Models.Favs;
import com.mycompany.MANDOWEB.Models.Noticias;
import com.mycompany.MANDOWEB.Models.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "FavoritosController", urlPatterns = {"/FavoritosController"})
public class FavoritosController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("idUser");
        List<Favs> favs = FavsDAO.getFavs(Integer.parseInt(id, 10));
        request.setAttribute("favs", favs);

        request.getRequestDispatcher("favs.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idNews = request.getParameter("idNews");
        String idUser = request.getParameter("idUser");
        
        Noticias noti = NoticiasDAO.getNew(Integer.parseInt(idNews));
        FavsDAO.insertFavs(new Favs(Integer.parseInt(idUser), noti));

        request.getRequestDispatcher("NoticiaMostrarController?id=" + idNews).forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
