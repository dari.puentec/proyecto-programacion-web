
package com.mycompany.MANDOWEB.Models;

public class Categorias {
    private int id;
    private String categoria;

    public Categorias(int id) {
        this.id = id;
    }
    
    public Categorias(int id, String categoria) {
        this.id = id;
        this.categoria = categoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    
    
}
