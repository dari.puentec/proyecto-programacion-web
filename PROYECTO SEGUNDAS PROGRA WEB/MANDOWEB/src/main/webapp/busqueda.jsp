<%@page import="com.mycompany.MANDOWEB.Models.Usuario"%>
<%@page import="com.mycompany.MANDOWEB.Models.Noticias"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Noticias> noticia = (List<Noticias>) request.getAttribute("noticia");
       Usuario sesion = (Usuario) session.getAttribute("logIn");
%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="meta.jsp"/>
        <title>Busqueda</title>
   <link rel="stylesheet" href="Styles/main.css">
    </head>
    <body>
        <jsp:include page="navbar.jsp"/>
        <div class="fondoforo">
            <div class="container"> <div class="row">



                <div class="row row-cols-1 row-cols-md-3"style="margin-top:20px">
                    <%
                        for (Noticias noti : noticia) {
                    %>
                    <div class="col mb-4">
                        <div class="card h-100">
                            <a href="NoticiaMostrarController?id=<%= noti.getId()%>&idUser=<%if (sesion != null) {%><%=sesion.getIdUsua()%><%}%>">
                                <img src="<%= noti.getPath1()%>" class="card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title"><%= noti.getTitulo()%></h5>
                                    <p class="card-text"><%= noti.getDescrip()%></p>
                                    <p class="card-text"><small class="text-muted"><%= noti.getUser().getNombre_Usua()%> </small><small class="text-muted"><%= noti.getFecha()%></small></p>
                                    <p class="card-text"><small class="text-muted"><%= noti.getCategoria().getCategoria()%> </small></p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>

</div> 
            </div>
        </div>
    </body>
</html>
