
<%@page import="com.mycompany.MANDOWEB.Models.Categorias"%>
<%@page import="com.mycompany.MANDOWEB.Models.Favs"%>
<%@page import="com.mycompany.MANDOWEB.Models.Comentarios"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.MANDOWEB.Models.Usuario"%>
<%@page import="com.mycompany.MANDOWEB.Models.Noticias"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Categorias> categorias = (List<Categorias>) request.getAttribute("categories");
    Noticias noticia = (Noticias) request.getAttribute("noticia");
    Usuario sesion = (Usuario) session.getAttribute("logIn");
%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="meta.jsp"/>
        <title>Modificar <%= noticia.getTitulo()%></title>
          <link rel="stylesheet" href="Styles/main.css">
    </head>
    <body>
        <jsp:include page="navbar.jsp"/>
        <div class="fondomodi">
            <div class="container"style="background-image: linear-gradient(rgba(350,200,233, 0.4),rgba(250,50,0, 0.4)), url(img/pinkyfond.jpg); height:100% ;margin-top:20px; border-radius: 15px 15px 15px 15px;background-size: cover;"">
            <div class="row">

                <div class="form-group"style="margin-top: 45px; margin-left:10px">
                    <h1>Modificar Noticia</h1>
                    <label for="redes"style="margin-left: 450px">Cambios por hacer</label>
                    <textarea class="form-control" name="redes" id="redes" 
                              rows="3" style="margin-left: 250px;color:magenta" disabled><%= noticia.getCambio()%></textarea>
                </div>
                <form class="col-12" method="POST" enctype="multipart/form-data" action="NoticiaCanceladoControlle">
                    <div class="form-group">
                        <label for="title">Título</label>
                        <input type="text" class="form-control" name="title" id="title" value="<%= noticia.getTitulo()%>">
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción Corta</label>
                        <textarea class="form-control" name="descripcion" id="descripcion" rows="3"><%= noticia.getDescrip()%></textarea>
                    </div>
                    <div class="form-group">
                        <label for="noticia">Noticia</label>
                        <textarea class="form-control" name="noticia" id="noticia" rows="3"><%= noticia.getNoticia()%></textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">Categoría</label>
                        <select class="form-control" name="category" id="category">
                            <option value="-1">Seleccione una categoría</option>
                            <%
                                for (Categorias categori : categorias) {
                            %>
                            <option value="<%= categori.getId()%>"><%= categori.getCategoria()%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <input type="hidden" name="idNoti" value="<%= noticia.getId()%>">
                    <input type="hidden" name="IdUsua" value="<%= sesion.getIdUsua()%>">
                    <div class="form-group">
                        <label for="image">Imagen 1</label>
                        <input type="file" class="form-control" name="image" id="image">
                        <small class="form-text text-muted">Tamaño máximo de archivo: 5Mb.</small>
                    </div>
                    <div class="form-group">
                        <label for="image2">Imagen 2</label>
                        <input type="file" class="form-control" name="image2" id="image2">
                        <small class="form-text text-muted">Tamaño máximo de archivo: 5Mb.</small>
                    </div>
                    <div class="form-group">
                        <label for="image3">Imagen 3</label>
                        <input type="file" class="form-control" name="image3" id="image3">
                        <small class="form-text text-muted">Tamaño máximo de archivo: 5Mb.</small>
                    </div>
                    <div class="form-group">
                        <label for="video">Vídeo</label>
                        <input type="file" class="form-control" name="video" id="video">
                        <small class="form-text text-muted">Tamaño máximo de archivo: 5Mb.</small>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" style="background-color: green" value="Subir noticia">
                    </div>
                </form>

            </div>
        </div>
                    </div>
    </body>
</html>
