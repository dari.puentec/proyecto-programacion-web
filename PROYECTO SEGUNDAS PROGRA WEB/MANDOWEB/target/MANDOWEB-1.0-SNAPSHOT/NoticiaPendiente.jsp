
<%@page import="com.mycompany.MANDOWEB.Models.Favs"%>
<%@page import="com.mycompany.MANDOWEB.Models.Comentarios"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.MANDOWEB.Models.Usuario"%>
<%@page import="com.mycompany.MANDOWEB.Models.Noticias"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Noticias noticia = (Noticias) request.getAttribute("noticia");
%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="meta.jsp"/>
        <title><%= noticia.getTitulo()%></title>
          <link rel="stylesheet" href="Styles/main.css">
    </head>
    <body>
        <jsp:include page="navbar.jsp"/>
         <div class="fondoperfil">

        <div class="container">
            <div class="row" style="background-image: linear-gradient(rgba(350,200,233, 0.4),rgba(350,200,233, 0.4)), url(img/pinkyfond.jpg); height:100% ;margin-top:10px; border-radius: 15px 15px 15px 15px;background-size: cover;"">


                <h1 class="col-12"align="center" class="col-12"style="background-image:  linear-gradient(rgba(100,0,100, 0),rgba(0, 0, 0, 0));height:100%; border-radius: 15px 15px 0px 0px;background-size: cover"><%= noticia.getTitulo()%></h1>
                <small class="col-4 text-muted"align="right" >Categoría: <%= noticia.getCategoria().getCategoria()%> </small>
                <small class="col-4 text-muted"align="center">Creado por: <%= noticia.getUser().getNombre_Usua()%> </small>
                <small class="col-2 text-muted" align="right"> el día <%= noticia.getFecha()%> </small>
                <div class="carousel slide col-12" data-ride="carousel"style="margin-top:10px">
                    <div class="carousel-inner"style="margin-top:10px; border-radius: 15px 15px 15px 15px">
                        <div class="carousel-item active">
                            <img src="<%= noticia.getPath1()%>" class="d-block w-100" style="border-radius: 15px 15px 15px 15px">
                        </div>
                    </div>
                </div>
                <article align="center"class="col-12"style="background-image: linear-gradient(rgba(255, 181, 248, 0.4),rgba(35,233,233, 0.4)), url(img/pinkyfond.jpg); margin-top:10px">
                    <p>
                        <%= noticia.getDescrip()%>
                    </p>
                </article>
                 <div class="carousel slide col-12" data-ride="carousel"style="margin-top:10px">
                    <div class="carousel-inner"style=" border-radius: 15px 15px 15px 15px">
                        <div class="carousel-item active">
                            <img src="<%= noticia.getPath2()%>" class="d-block w-100" alt="...">
                        </div>
                    </div>
                </div>
                 <article align="center"class="col-12"style="background-image: linear-gradient(rgba(255, 181, 248, 0.4),rgba(35,233,233, 0.4)), url(img/pinkyfond.jpg); margin-top:10px">
                    <p>
                        <%= noticia.getNoticia()%>
                    </p>
                </article>
                  <div class="carousel slide col-12" data-ride="carousel"style="margin-top:10px">
                    <div class="carousel-inner"style="border-radius: 15px 15px 15px 15px">
                        <div class="carousel-item active">
                            <img src="<%= noticia.getPath3()%>" class="d-block w-100" alt="...">
                        </div>
                    </div>
                </div>
                <%

                    if (noticia.getPathv() != null) {
                %>
                <video class="col-12" controls>
                    <source src="<%= noticia.getPathv()%>" type="video/mp4">
                    Your browser does not support the video tag.
                </video> 
                <%
                    }
                %>

                <form action="NoticiaEstadoController" align="center">
                    <h3>Si considera que la noticia está bien y no necesita ningún cambio, confirme que 
                        <input style="margin-left: auto"type="submit" class="btn btn-primary" value="ACEPTA LA NOTICIA"> </h3>
                    <input type="hidden" name="idNews" value="<%= noticia.getId()%>">
                </form>
                <form method="POST" action="NoticiaEstadoController">
                    <div class="form-group">
                        <h3>En cambio, si a su consideración, debe realizarse algún cambio</h3>
                        <textarea class="form-control" name="cambio" id="commentary"
                                  placeholder="Escríbalo aquí"></textarea>
                        <input type="submit" class="btn btn-primary" value="y confirme que CANCELA LA NOTICIA">
                        <input type="hidden" name="idNews" value="<%= noticia.getId()%>">
                    </div>
                </form>

            </div>
        </div>
                     </div>
    </body>
</html>
