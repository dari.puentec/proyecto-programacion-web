

<%@page import="com.mycompany.MANDOWEB.Models.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Usuario sesion = (Usuario) session.getAttribute("logIn");
%>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PROYECTO </title>
    <link rel="stylesheet" href="Styles/main.css">

<nav class="navbar navbar-expand-lg navbar-light bg-light"style="background-image: linear-gradient(rgba(255, 181, 248, 0.6),rgba(35,233,233, 0.6)), url(img/imgnavbar2.jpg);background-size: 18%;"> 
 <a class="navbar-brand " ><img src="img/m.png" alt="" class="imag" href="indexController"></a>
       
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="indexController">Noticias <span class="sr-only">(current)</span></a>
           
            
            <% 
               if(sesion == null) {
        %>
           <li class="nav-item ">
                <a class="nav-link" href="Sesion">Unete a la comunidad <span class="sr-only"></span></a>
            </li>
            
            <%  
            } 
        %>
       
         
            <%
                if (sesion != null) {
            %>
            <li class="nav-item" >
                <a class="nav-link" href="FavoritosController?idUser=<%= sesion.getIdUsua()%>" >Favoritos</a>
            </li>

            <%
                if (sesion.getTipo() == 2 || sesion.getTipo() == 4 || sesion.getTipo() == 5) {
            %>
            <li class="nav-item">
                <a class="nav-link" href="NoticiaCrearController">Crear Nueva Noticia</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="NoticiaCancelados?idCreador=<%= sesion.getIdUsua()%>" >Noticias a Modificar</a>
            </li>
            <%
                }
            %>
            <%
                if (sesion.getTipo() == 2 || sesion.getTipo() == 5) {
            %>
            <li class="nav-item">
                <a class="nav-link" href="NoticiaPendienteController">Noticias Pendientes</a>
            </li>
            <%
                }
            %>
            <%
                }
            %>
        </ul>
         <%  
            if (sesion != null) {
        %>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown avatar">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i> <%= sesion.getNombre_Usua()%> 
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                        <a class="dropdown-item" href="Perfil.jsp">Perfil</a>
                        <a class="dropdown-item" href="CerrarSesion">Cerrar Sesión</a>
                    </div>
                </li>
                <li class="nav-item avatar">
                    <img src="<%= sesion.getPath()%>" class="rounded-circle z-depth-0"
                         alt="avatar image" height="45" style="max-height: 45px;max-width: 45px;min-width: 45px;min-height: 45px">
                </li>
                <li class="nav-item avatar"></li>
            </ul>
        </div>
        <%
            }
        %>
        <form class="form-inline my-2 my-lg-0" method="POST" action="BuscadorController">
            <input class="form-control mr-sm-2" type="search" placeholder="Buscar noticia" aria-label="Search" name="buscar">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" style="color: #FFF;">Buscar</button>
        </form>
    </div>
</nav>