<%-- 
    Document   : news
    Created on : 24/11/2020, 07:32:41 PM
    Author     : magoc
--%>
<%@page import="java.util.List"%>
<%@page import="com.pw.pw01prb.models.Comment"%>
<%@page import="com.pw.pw01prb.models.New"%>
<%@page import="com.pw.pw01prb.models.User"%>
<%
    New element = (New) request.getAttribute("New");
    List<Comment> comments = (List<Comment>) request.getAttribute("Comments");
    User user = (User) request.getAttribute("UserLogueado");                                    
 %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   
    <body>
        <jsp:include page="navbar.jsp" />  
     
                <div class="imageen">
        <div class="container">
            <div style="background-color:white;">
                <div class="row"">
                <h1 class="col-12"><%= element.getTitle()%></h1>
                <small class="text-muted col-12">Categoria <%= element.getCategory().getName()%></small>

                <div id="carouselExampleControls" class="carousel slide col-12" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img WIDTH="200" height="500"src="<%= element.getPathImage()%>" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <p class="col-12">
                    <%= element.getDescription()%>
                </p>
            </div>
            <div class="row">
                <div class="col-12" id="comment-container">
                    <p align="center"><h3>Comentarios</h3></p>
                    <form class="col-12" method="POST" action="CommentsController">
                        <div class="from-group">
                            <textarea class="form-control" name="content"></textarea>
                            <input type="hidden" name="idNews" value="<%= element.getId()%>">
                            <input type="hidden" name="parent" value="-1">
                            <input type="hidden" name="UserLogueado" value ="<%= user != null ? user.getId() : -1%>">
                        </div>
                        <div class="from-group">
                            <input type="submit" class="btn btn-success" value="Comentar">
                        </div>
                    </form>
                    <%
                        for (Comment comment : comments) {
                    %>
                
                    <div class="media"> 
                         <% 
                            if(user != null && !(user.getRol() == 1 || user.getRol() == 3)) {
                        %>
                                <a class="delete btn btn-danger" href="DeleteCommentController?idNews=<%= element.getId()%>&id=<%= comment.getId()%>&UserLogueado=<%= user != null ? user.getId() : -1%>">Eliminar</a>
                         <%
                            }
                        %>
                          <div class="media-body">
                            <h5 class="mt-0"><%= comment.getUser().getUsername()%></h5>
                            <%= comment.getContent()%>

                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
        </div>
    </body>
</html>
