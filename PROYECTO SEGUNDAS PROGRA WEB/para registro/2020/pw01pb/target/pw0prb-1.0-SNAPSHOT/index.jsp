<%@page import="com.pw.pw01prb.models.New"%>
<%@page import="com.pw.pw01prb.models.Category"%>
<%@page import="com.pw.pw01prb.models.User"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
    List<New> news = (List<New>) request.getAttribute("News");
    User user = (User) request.getAttribute("UserLogueado");  
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
    </head>
    <body>
        <jsp:include page="navbar.jsp" />  
    <div class="imageen">
        <div class="container">
         <p align="center">  <title > Noticias</title> </p>
  
         <div class="row" style="background-color:white;" >
                <h2 class="col-12">Noticias</h2><br>
                <%
                    for (New element : news) {
                %>
                <div class="card mb-3 col-12"  id="table">
                    <a href="ShowNewsController?id=<%= element.getId() %>&UserLogueado=<%= user != null ? user.getId() : -1%>">
                        <div class="row no-gutters">
                            <div class="col-md-4" >
                                <img   src="<%= element.getPathImage()%>" class="card-img"  alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title"><%= element.getTitle()%></h5>
                                    <p class="card-text"><%= element.getDescription()%></p>
                                    <p class="card-text"><small class="text-muted"><%= element.getCategory().getName()%></small></p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <%
                    }
                %>
            </div>
        </div>
             </div>
    </body>
</html>
