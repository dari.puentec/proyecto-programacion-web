/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.controllers;

import com.pw.pw01prb.dao.CategoryDAO;
import com.pw.pw01prb.dao.CommentDAO;
import com.pw.pw01prb.dao.NewDAO;
import com.pw.pw01prb.dao.UserDAO;
import com.pw.pw01prb.models.Category;
import com.pw.pw01prb.models.Comment;
import com.pw.pw01prb.models.New;
import com.pw.pw01prb.models.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author magoc
 */
@WebServlet(name = "ShowNewsController", urlPatterns = {"/ShowNewsController"})
public class ShowNewsController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        int userID = Integer.parseInt(request.getParameter("UserLogueado"));
        New element = NewDAO.getNews(id);
        User user = null;
        List<Category> categories = null;
        List<Comment> comments = CommentDAO.getCommentsByNews(id);
        try {
            categories = CategoryDAO.getCategories();
            user = UserDAO.getUser(userID);
        } catch (SQLException ex) {
            Logger.getLogger(indexController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.setAttribute("UserLogueado", user);
        request.setAttribute("Categories", categories);
        request.setAttribute("New", element);
        request.setAttribute("Comments", comments);
        request.getRequestDispatcher("news.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"), 10);
        New element = NewDAO.getNews(id);
        List<Category> categories = null;
        List<Comment> comments = CommentDAO.getCommentsByNews(id);
        try {
            categories = CategoryDAO.getCategories();
        } catch (SQLException ex) {
            Logger.getLogger(indexController.class.getName()).log(Level.SEVERE, null, ex);
        }
        User user = (User) request.getAttribute("UserLogueado");
        request.setAttribute("Categories", categories);
        request.setAttribute("New", element);
        request.setAttribute("Comments", comments);
        request.getRequestDispatcher("news.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
