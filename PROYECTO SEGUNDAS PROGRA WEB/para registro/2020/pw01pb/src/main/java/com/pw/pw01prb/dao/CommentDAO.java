/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.dao;

import com.pw.pw01prb.models.Comment;
import com.pw.pw01prb.models.New;
import com.pw.pw01prb.models.User;
import com.pw.pw01prb.utils.DbConnection;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author magoc
 */
public class CommentDAO {

//    CREATE TABLE `comment` (
//  `idcomment` int NOT NULL AUTO_INCREMENT,
//  `content` text,
//  `iduser` int unsigned DEFAULT NULL,
//  `parent` int DEFAULT NULL,
//  `idnews` int DEFAULT NULL,
//  PRIMARY KEY (`idcomment`),
//  UNIQUE KEY `idcomments_UNIQUE` (`idcomment`),
//  KEY `fk_news_comment_idx` (`idnews`),
//  KEY `fk_user_comment_idx` (`iduser`),
//  KEY `fk_comment_comment_idx` (`parent`),
//  CONSTRAINT `fk_news_comment` FOREIGN KEY (`idnews`) REFERENCES `new` (`idnew`),
//  CONSTRAINT `fk_user_comment` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
//DELIMITER $$
//CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertComment`(
//IN `pContent` text,
//IN `pIduser` int unsigned,
//IN `pParent` int,
//IN `pIdnews` int)
//BEGIN
//INSERT INTO `comment`
//(`content`,
//`iduser`,
//`parent`,
//`idnews`)
//VALUES
//(pContent,
//pIduser,
//pParent,
//pIdnews);
//
//END$$
//DELIMITER ;
    public static int insertComment(Comment comment) {
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "CALL InsertComment(?,?,?,?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, comment.getContent());
            statement.setInt(2, comment.getUser().getId());
            statement.setInt(3, comment.getParent());
            statement.setInt(4, comment.getNews());

            return statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

//    USE `pw01`;
//DROP procedure IF EXISTS `GetCommentsByNews`;
//
//DELIMITER $$
//USE `pw01`$$
//CREATE PROCEDURE `GetCommentsByNews` (
//IN `pIdnews` int
//)
//BEGIN
//SELECT `comment`.`idcomment`,
//    `comment`.`content`,
//    `comment`.`iduser`,
//    `comment`.`parent`,
//    `comment`.`idnews`
//FROM `pw01`.`comment`
//WHERE `comment`.`idnews` = pIdnews;
//END$$
//
//DELIMITER ;
//
    public static List<Comment> getCommentsByNews(int idNews) {
        List<Comment> comments = new ArrayList<>();
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "CALL GetCommentsByNews(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setInt(1, idNews);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int id = result.getInt(1);
                String content = result.getString(2);
                int idUser = result.getInt(3);
                int parent = result.getInt(4);
                int idNew = result.getInt(5);
                User user = UserDAO.getUser(idUser);
                comments.add(new Comment(id, content, user, idNews, parent));
            }
            return comments;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return comments;
    }
    
    
//    USE `pw01`;
//DROP procedure IF EXISTS `DeleteComment`;
//
//DELIMITER $$
//USE `pw01`$$
//CREATE PROCEDURE `DeleteComment` (
//IN `pIdcomment` int)
//BEGIN
//DELETE FROM `pw01`.`comment`
//WHERE idcomment = pIdcomment;
//
//END$$
//
//DELIMITER ;
//

    public static int deleteComment(int id) {
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "CALL DeleteComment(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setInt(1, id);

            return statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
}
