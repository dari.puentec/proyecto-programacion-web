/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.dao;

import com.pw.pw01prb.models.User;
import com.pw.pw01prb.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author magoc
 */
public class UserDAO {

//    CREATE TABLE `pw01`.`user` (
//  `iduser` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//  `username` VARCHAR(45) NULL,
//  `password` VARCHAR(100) NULL,
//  PRIMARY KEY (`iduser`),
//  UNIQUE INDEX `iduser_UNIQUE` (`iduser` ASC) VISIBLE,
//  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE);
//    USE `pw01`;
//    DROP procedure IF EXISTS `InsertUser`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `InsertUser` (
//    IN `pUsername` varchar(45),
//    IN `pPassword` varchar(100)
//    )
//    BEGIN
//    INSERT INTO user
//    (`username`,
//    `password`)
//    VALUES
//    (pUsername,
//    pPassword);
//
//    END$$
//
//    DELIMITER ;
//
    public static int insertUser(User user) {
        try {
            Connection con = DbConnection.getConnection();
            String sql = "call InsertUser(?,?,?,?)";
            CallableStatement statement = con.prepareCall(sql);
           statement.setString(1, user.getUsername());
            // El segundo por la contraseña
            statement.setString(2, user.getPassword());
            // El tercero por la url de la imagen
            statement.setString(4, user.getUrlImage());
            // Ultimo el email
             statement.setString(3, user.getEmail());
 // Ejecuta el Statement y retorna cuantos registros
            // fueron actualizados
            return statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

//    USE `pw01`;
//    DROP procedure IF EXISTS `LogInUser`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `LogInUser` (
//    IN `pUsername` varchar(45),
//    IN `pPassword` varchar(100)
//    )
//    BEGIN
//    SELECT u.iduser as ID,
//        u.username
//    FROM user u
//    WHERE u.username = pUsername
//    AND u.password = pPassword;
//
//    END$$
//
//    DELIMITER ;
//

    public static User logInUser(User user) {
        try {
            Connection con = DbConnection.getConnection();
            String sql = "call LogInUser(?,?,?,?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            
          
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
              String name = resultSet.getString("nameUser");
                String email = resultSet.getString("emailUser");
                String password = resultSet.getString("password");
                String urlImage = resultSet.getString("urlImage");
                // Agregamos el usuario a la lista
                 return new User(name, email,password,urlImage);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
//    USE `pw01`;
//DROP procedure IF EXISTS `GetUser`;
//
//DELIMITER $$
//USE `pw01`$$
//CREATE PROCEDURE `GetUser` (
//IN `pIduser` int unsigned
//)
//BEGIN
//SELECT `user`.`iduser`,
//    `user`.`username`
//FROM `pw01`.`user`
//WHERE `user`.`iduser` = pIduser;
//
//END$$
//
//DELIMITER ;
//

    public static User getUser(int idUser) {
        try {
            Connection con = DbConnection.getConnection();
            String sql = "CALL GetUser1(?);";
            CallableStatement statement = con.prepareCall(sql);
            statement.setInt(1, idUser);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                int id = resultSet.getInt(1);
                  int rol = resultSet.getInt(3);
                String username = resultSet.getString(2);
                return new User(id, username,rol);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

}
