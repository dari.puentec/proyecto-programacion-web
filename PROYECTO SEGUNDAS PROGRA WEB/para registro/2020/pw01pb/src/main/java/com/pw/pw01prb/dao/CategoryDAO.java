/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.dao;

import com.pw.pw01prb.models.Category;
import com.pw.pw01prb.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author magoc
 */
public class CategoryDAO {
//    CREATE TABLE `pw01`.`category` (
//  `idcategory` INT NOT NULL AUTO_INCREMENT,
//  `name` VARCHAR(45) NULL,
//  `order` INT NULL,
//  `parent` INT NULL,
//  PRIMARY KEY (`idcategory`),
//  UNIQUE INDEX `idcategory_UNIQUE` (`idcategory` ASC) VISIBLE);
//    ALTER TABLE `pw01`.`category` 
//    ADD INDEX `fk_category_parent_idx` (`parent` ASC) VISIBLE;
//    ;
//    ALTER TABLE `pw01`.`category` 
//    ADD CONSTRAINT `fk_category_parent`
//      FOREIGN KEY (`parent`)
//      REFERENCES `pw01`.`category` (`idcategory`)
//      ON DELETE NO ACTION
//      ON UPDATE NO ACTION;

//    USE `pw01`;
//    DROP procedure IF EXISTS `GetCategories`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `GetCategories` ()
//    BEGIN
//            SELECT `category`.`idcategory`,
//                    `category`.`name`,
//                    `category`.`order`,
//                    `category`.`parent`
//            FROM `category`
//        ORDER BY `category`.`order`,`category`.`name`;
//
//    END$$
//
//    DELIMITER ;
    public static List<Category> getCategories() throws SQLException {
        List<Category> categories = new ArrayList<>();
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "call GetCategories()";
            CallableStatement statement = con.prepareCall(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int id = result.getInt("idcategory");
                String name = result.getString("name");
                int order = result.getInt("order");
                int parent = result.getInt("parent");
                categories.add(new Category(id, name, order, parent));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return categories;
    }
    
//    USE `pw01`;
//    DROP procedure IF EXISTS `GetCategory`;
//
//    DELIMITER $$
//    USE `pw01`$$
//    CREATE PROCEDURE `GetCategory` (
//    IN `pIdcategory` int
//    )
//    BEGIN
//    SELECT `category`.`idcategory`,
//        `category`.`name`,
//        `category`.`order`,
//        `category`.`parent`
//    FROM `pw01`.`category`
//    WHERE `category`.`idcategory` = pIdcategory;
//    END$$
//
//    DELIMITER ;
//

    public static Category getCategory(int id) throws SQLException {
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "call GetCategory(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                id = result.getInt("idcategory");
                String name = result.getString("name");
                int order = result.getInt("order");
                int parent = result.getInt("parent");
                return new Category(id, name, order, parent);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return null;
    }
}
