/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.controllers;

import com.pw.pw01prb.dao.CommentDAO;
import com.pw.pw01prb.models.Comment;
import com.pw.pw01prb.models.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author magoc
 */
@WebServlet(name = "CommentsController", urlPatterns = {"/CommentsController"})
public class CommentsController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String content = request.getParameter("content");
        String id = request.getParameter("idNews");
        String parent = request.getParameter("parent");
        int idNews = Integer.parseInt(id, 10);
        int idParent = Integer.parseInt(parent, 10);
        int idUser = Integer.parseInt(request.getParameter("UserLogueado"));
        CommentDAO.insertComment(new Comment(content, new User(idUser), idNews, idParent));
        request.getRequestDispatcher("/ShowNewsController?id=" + id).forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
