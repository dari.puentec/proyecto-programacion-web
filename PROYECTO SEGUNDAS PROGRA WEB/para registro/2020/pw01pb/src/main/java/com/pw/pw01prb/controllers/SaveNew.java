/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.controllers;

import com.pw.pw01prb.dao.CategoryDAO;
import com.pw.pw01prb.dao.NewDAO;
import com.pw.pw01prb.models.Category;
import com.pw.pw01prb.models.New;
import com.pw.pw01prb.utils.FileUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author magoc
 */
@WebServlet(name = "SaveNew", urlPatterns = {"/SaveNew"})
@MultipartConfig(maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 25)
public class SaveNew extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String stringCategory = request.getParameter("category");
        int category = Integer.parseInt(stringCategory, 10);
        Part file = request.getPart("image");

        String nameImage = file.getName() + System.currentTimeMillis() + FileUtils.GetExtension(file.getContentType());
        String path = request.getServletContext().getRealPath("");
        String fullPath = path +  FileUtils.RUTE_USER_IMAGE + "/" + nameImage;

        file.write(fullPath);

        New element = new New(title, description, new Category(category), FileUtils.RUTE_USER_IMAGE + "/" + nameImage);

        if (NewDAO.insertNew(element) == 1) {
            List<Category> categories = null;
            List<New> news = null;
            try {
                categories = CategoryDAO.getCategories();
            } catch (SQLException ex) {
                Logger.getLogger(indexController.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                news = NewDAO.getNews();
            } catch (SQLException ex) {
                Logger.getLogger(indexController.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("Categories", categories);
            request.setAttribute("News", news);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
            response.sendRedirect("fail.jsp");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
