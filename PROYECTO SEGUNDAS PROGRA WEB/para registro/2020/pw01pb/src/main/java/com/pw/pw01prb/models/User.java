/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.models;

/**
 * Modelo de usuario
 *
 * @author magoc
 */
public class User {

    private int id;
    private String email;
    private String username;
    private String password;
    private String urlImage; 
    private int rol;   

    public User() {
    }

    public User(int id) {
        this.id = id;
    }

    public User(String username, String email, String password, String urlImage, int rol) {
        this.username = username;
        this.email = email;

        this.password = password;
        this.urlImage = urlImage;
        this.rol = rol;
    }
public User(int id, String username,int rol) {
        this.id = id;
        this.username = username;
        this.rol = rol;
    }

    public User(String username, String email, String password, String urlImage) {
        this.username = username;
        this.email = email;

        this.password = password;
        this.urlImage = urlImage;
    
    }

   //  public User(String username, String password, int rol ) {
    public User(String username, String password ) {
        this.username = username;
        this.password = password;
       // this.rol = rol;
    }

    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }

   // public User(int id, String username, String password , int rol) {
        public User(int id, String username, String password ) {
        this.id = id;
        this.username = username;
        this.password = password;
      //  this.rol = rol;
    }

    public int getId() {
        return id;
    }
    // public int getrol() {
      //  return rol;
    //}
    public void setId(int id) {
        this.id = id;
    }
  //   public void setrol(int rol) {
     //   this.rol = rol;
    //}
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

}
