/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.models;

/**
 *
 * @author magoc
 */
public class New {

    private int id;
    private String title;
    private String description;
    private Category category;
    private String pathImage;

    public New() {
    }

    public New(String title, String description, Category category, String pathImage) {
        this.title = title;
        this.description = description;
        this.category = category;
        this.pathImage = pathImage;
    }

    public New(int id, String title, String description, Category category, String pathImage) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.category = category;
        this.pathImage = pathImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

}
