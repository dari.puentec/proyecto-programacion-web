/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.controllers;

import com.pw.pw01prb.dao.NewDAO;
import com.pw.pw01prb.dao.UserDAO;
import com.pw.pw01prb.models.New;
import com.pw.pw01prb.models.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author magoc
 */
@WebServlet(name = "LogInController", urlPatterns = {"/LogInController"})
public class LogInController extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
         
        User user;
        user = UserDAO.logInUser(new User(username, password));
        
        if (user != null) {
            //HttpSession session = request.getSession();
            //session.setAttribute("Username", result.getUsername());
          //  session.setAttribute("ID_User", result.getId());
          List<New> news = null;
        try {
            news = NewDAO.getNews();
        } catch (SQLException ex) {
            Logger.getLogger(LogOffController.class.getName()).log(Level.SEVERE, null, ex);
        }
            request.setAttribute("News", news);
            request.setAttribute("UserLogueado", user);

            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
            response.sendRedirect("fail.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
