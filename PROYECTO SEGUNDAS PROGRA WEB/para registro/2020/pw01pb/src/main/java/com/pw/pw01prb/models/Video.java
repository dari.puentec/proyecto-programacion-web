/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.models;

/**
 *
 * @author andru
 */
public class Video {
   private int id;
    private String title;
    private String description;
    private Category category;
    private String pathVideo;

    public Video() {
    }

    public Video(String title, String description, Category category, String pathVideo) {
        this.title = title;
        this.description = description;
        this.category = category;
        this.pathVideo = pathVideo;
    }

    public Video(int id, String title, String description, Category category, String pathVideo) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.category = category;
        this.pathVideo = pathVideo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getPathVideo() {
        return pathVideo;
    }

    public void setPathVideo(String pathVideo) {
        this.pathVideo = pathVideo;
    }

}
