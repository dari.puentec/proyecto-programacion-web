/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.controllers;

import com.pw.pw01prb.dao.NewDAO;
import com.pw.pw01prb.dao.UserDAO;
import com.pw.pw01prb.models.New;
import com.pw.pw01prb.models.User;
import com.pw.pw01prb.utils.FileUtils;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author magoc
 */
@WebServlet(name = "SignInController", urlPatterns = {"/SignInController"})
@MultipartConfig(maxFileSize = 1000 * 1000 * 5, maxRequestSize = 1000 * 1000 * 25, fileSizeThreshold = 1000 * 1000)
public class SignInController extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String username = request.getParameter("name");
        // Obtenemos el contraseña debe coincidir con el name del input
        String password = request.getParameter("password");
        
        String email = request.getParameter("email");

        String path = request.getServletContext().getRealPath("");
        // Obtenemos la Direccion donde deseamos guardarlo
        File fileSaveDir = new File(path + FileUtils.RUTE_USER_IMAGE);
        // Sino existe el directorio la creamos
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }
        // Obtenemos la imagen, debe coincidir con el name del input
        Part file = request.getPart("image");
        String contentType = file.getContentType();
        // Remplazamos el nombre que tiene para que no existan duplicados
        String nameImage = file.getName() + System.currentTimeMillis() + FileUtils.GetExtension(contentType);
        String fullPath = path + FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
        // Copiamos la imagen en la ruta especificada
        file.write(fullPath);
        User user;
        user = UserDAO.logInUser(new User(username, password));
        
        if (user == null && UserDAO.insertUser(new User(username, email, password,path )) == 1) {
            // Se Inserto
            List<New> news = null;
            try {
                news = NewDAO.getNews();
            } catch (SQLException ex) {
                Logger.getLogger(SignInController.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("News", news);
            request.getRequestDispatcher("index.jsp").forward(request, response);
            //response.sendRedirect("index.jsp");
        } else {
            // No se Inserto
            response.sendRedirect("fail.jsp");
        }
//        response.sendRedirect("index.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
