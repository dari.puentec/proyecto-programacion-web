/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw01prb.models;

import java.util.List;

/**
 *
 * @author magoc
 */
public class Comment {

    private int id;
    private String content;
    // El Usuario que escribio la noticia
    private User user;
    // La noticia a la que pertenecemos
    private int news;
    // El comentario al cual pertenecemos
    private int parent;
    // Relacion inversa de los comentarios
    private List<Comment> answers;

    public Comment() {
    }

    public Comment(int id) {
        this.id = id;
    }

    public Comment(int id, String content, User user, int news, int parent) {
        this.id = id;
        this.content = content;
        this.user = user;
        this.news = news;
        this.parent = parent;
    }

    public Comment(String content, User user, int news, int parent) {
        this.content = content;
        this.user = user;
        this.news = news;
        this.parent = parent;
    }

    public Comment(int id, String content, User user, int news, int parent, List<Comment> answers) {
        this.id = id;
        this.content = content;
        this.user = user;
        this.news = news;
        this.parent = parent;
        this.answers = answers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getNews() {
        return news;
    }

    public void setNews(int news) {
        this.news = news;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public List<Comment> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Comment> answers) {
        this.answers = answers;
    }

}
