
<%@page import="com.pw.pw01prb.models.Category"%>
<%@page import="com.pw.pw01prb.models.User"%>
<%@page import="java.util.List"%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>    
   <script src= "mainboostrap.js"> </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PROYECTO </title>
    <link rel="stylesheet" href="Styles/main.css">
 <% 
     User user = (User) request.getAttribute("UserLogueado");                                    
 %>
<nav class="navbar navbar-expand-lg navbar-light bg-light"style="background-image: linear-gradient(rgba(255, 181, 248, 0.6),rgba(35,233,233, 0.6)), url(img/imgnavbar2.jpg);background-size: 18%;"> 
 <a class="navbar-brand " ><img src="img/m.png" alt="" class="imag" href="index.jsp"></a>
       
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="indexController">Noticias <span class="sr-only">(current)</span></a>
           
            <li class="nav-item ">
                <a class="nav-link" href="AddVideos"> Videos <span class="sr-only"></span></a>
            </li>
            <% 
               if(user == null) {
        %>
           <li class="nav-item ">
                <a class="nav-link" href="Sesion">Unete a la comunidad <span class="sr-only"></span></a>
            </li>
            <%  
            }
         %>
         <% 
              if(user != null && user.getRol() == 0) {
        %>
            <li class="nav-item">
                <a class="nav-link" href="AddNew">Crear Noticia</a>
            </li>
            <%  
            }
         %>
        </ul>
      
         
        <% 
        
            if(user != null) {
        %>
        <h4><%= user.getUsername() %></h4>
              <form action="LogOffController" method="POST">   
                <input type="submit" class="btn btn-primary " value="Cerrar sesion">
          </form>

         <%  
            }
         %>
                    </div>
</nav>

        
        
        